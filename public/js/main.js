function loadMe() {
    hasSeenNewCard();

    if ($cardID == 11) {
        if (!$wasPlayerTurn) {
            setTimeout(function () {
                if (confirm("Robiłeś to?")) {
                    var i = 0;
                    do {
                        if (i == 0) {
                            sendToServer("/drinkOnJack");
                        } else {
                            sendToServer("/drinkOneMore");
                        }
                        i = 1;
                        alert("Pijesz!");

                    } while (!confirm("A znak był?"));
                    sendToServer("/room/" + $roomID + "/game/changeIsReady");
                } else {
                    sendToServer("/room/" + $roomID + "/game/changeIsReady");
                }
            }, 5000);
        } else {
            setTimeout(function () {
                alert("Czego nigdy nie robiłeś?");
                sendToServer("/room/" + $roomID + "/game/changeIsReady");
            }, 1000);

        }
    }


    if ($isPlayerTurnNow) {
        isReady();
    }
    
    if ($wasPlayerTurn) {
        if ($cardID == 2 || $cardID == 4 || $cardID == 7 || $cardID == 9 || $cardID == 10 || $cardID == 8) {
            setTimeout(function () {
                document.getElementById("list").style.display = "block";

            }, 5000);
        }
    }

    setInterval(function () {
        sendToServer("/room/" + $roomID + "/game/selectUserJson")
                .then(resp => resp.json())
                .then(resp => {
                    //console.log(resp);

                    if (resp == 1) {

                        var i = 0;
                        do {
                            if (i != 0) {
                                sendToServer("/drinkOneMore");
                            }
                            i = 1;
                            alert("Pijesz!");

                        } while (!confirm("A znak był?"));
                        sendToServer("/room/" + $roomID + "/game/changeIsReady");
                    }

                })
    }, 2000);

}

function sendToServer(url) {

    return fetch(url, {
        method: "POST",
        mode: "cors",
        cache: "no-cache",
        credentials: "same-origin",
        headers: {
            "Content-Type": "application/json; charset=utf-8"

        },
        redirect: "follow",
        referrer: "no-referrer"

    });
}

function hasSeenNewCard() {
    setInterval(function () {
        sendToServer("/room/" + $roomID + "/game/isNewCard")
                .then(resp => resp.json())
                .then(resp => {
                    //console.log(resp);

                    if (resp == 0) {
                        location.reload();
                    }

                })
    },
            1000);
}
function isReady() {
    setTimeout(function () {
        setInterval(function () {
            sendToServer("/room/" + $roomID + "/game/isReady")
                    .then(resp => resp.json())
                    .then(resp => {
                        //console.log(resp);

                        if (resp == 1) {
                            document.getElementById("card").style.pointerEvents = "";
                            document.getElementById("who").style.color = "green";

                        } else {
                            document.getElementById("card").style.pointerEvents = "none";
                            document.getElementById("who").style.color = "red";

                        }

                    })
        }, 1000);

    }, 5000);
}