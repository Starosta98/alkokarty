<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PlayersRepository")
 */
class Players
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    public $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    public $Name;

    /**
     * @ORM\Column(type="string", length=1)
     */
    private $Sex;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Players", cascade={"persist", "remove"})
     */
    private $Eight;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Rooms", inversedBy="PlayersID")
     */
    private $WhichRoom;

    /**
     * @ORM\Column(type="integer")
     */
    private $drinks;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $hasSeen;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $drinksNow;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isReady;

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->Name;
    }

    public function setName(?string $Name): self
    {
        $this->Name = $Name;

        return $this;
    }

    public function getSex(): ?string
    {
        return $this->Sex;
    }

    public function setSex(?string $Sex): self
    {
        $this->Sex = $Sex;

        return $this;
    }

    public function getEight(): ?self
    {
        return $this->Eight;
    }

    public function setEight(?self $Eight): self
    {
        $this->Eight = $Eight;

        return $this;
    }

    public function getWhichRoom(): ?Rooms
    {
        return $this->WhichRoom;
    }

    public function setWhichRoom(Rooms $WhichRoom): self
    {
        $this->WhichRoom = $WhichRoom;

        return $this;
    }

    public function getDrinks(): ?int
    {
        return $this->drinks;
    }

    public function setDrinks(int $drinks): self
    {
        $this->drinks = $drinks;

        return $this;
    }

  

    public function getHasSeen(): ?bool
    {
        return $this->hasSeen;
    }

    public function setHasSeen(?bool $hasSeen): self
    {
        $this->hasSeen = $hasSeen;

        return $this;
    }

    public function getDrinksNow(): ?bool
    {
        return $this->drinksNow;
    }

    public function setDrinksNow(?bool $drinksNow): self
    {
        $this->drinksNow = $drinksNow;

        return $this;
    }

    public function getIsReady(): ?bool
    {
        return $this->isReady;
    }

    public function setIsReady(?bool $isReady): self
    {
        $this->isReady = $isReady;

        return $this;
    }
}
