<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controller;

use App\Entity\Players;
use App\Entity\Rooms;
use App\Utils\Cards;
use App\Utils\Game;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\PhpBridgeSessionStorage;

class FetchController  extends Controller {
    
    
   /**
     * @Route("/room/{id}/startTheGame", name="startGame")
     * @Method({"GET", "POST"})
     */
    public function startTheGame( Request $request, $id, Game $game){
             $room = $this->getDoctrine()->getRepository(Rooms::class)->find($id);
        $entityManager = $this->getDoctrine()->getManager();
        $roomID = $room->getId();
        $players = $this->getDoctrine()->getRepository(Players::class)->findByID($id);
//           if ( $room->getIsStarted()==1){
//               return $this->redirectToRoute('end',array('id'=>$id) ); 
//            }
            //var_dump($request->get('howManyCards'));
            $game->start($request->get('howManyCards'));
            $room = $this->getDoctrine()->getRepository(Rooms::class)->find($id);
            $room->setIsStarted(1);
            $room->setGameEnd(0);
            $jsonContent = serialize($game->deck);
            $CP=$players[0];
            $room->setCurrentPlayer($CP);
            $room->setGame($jsonContent);
            foreach ( $players as $player){
                $player->setIsReady(1);
                
                $player->setHasSeen(1);
            }
            $entityManager->flush();
            return $this->redirectToRoute('game',array('id'=>$id) );  
            //return new Response("O chuj chodzi?");
        
    }

        /**
     * @Route("/room/{id}/game/click", name="click")
     * @Method({"GET", "POST"})
     */
    
    public function click($id, Game $game){
        $game->click($id);
        
        
//        $entityManager = $this->getDoctrine()->getManager();
//        $entityManager->flush();
        
        return $this->redirectToRoute('game', array('id'=>$id));
        
    }
     /**
     * @Route("/room/{id}/game/selectUser", name="select")
     * @Method({"GET", "POST"})
     */
    
    public function selectUser(Request $request, $id, Cards $oneCard){
        $postData = $request->get('player');
        $cardID = $request->get('cardID');

        if($cardID==12 || $cardID ==11 || $cardID==13 || $cardID==14){
            $oneCard->action(2,$postData);
            
        }
        else { 
            $oneCard->action($cardID, $postData);
        }
        
        return $this->redirectToRoute('game', array('id'=>$id));
        //return new Response($request->get('player'));
    }
   
    /**
     * @Route("/room/{id}/game/selectUserJson", name="selectJSON")
     * @Method({"GET", "POST"})
     */
    public function selectUserJson(){
//        $session = new Session(new PhpBridgeSessionStorage());
//        $session->start();
        $doesDrinkJSON=0;
        if(isset($_SESSION['idgracza'])){
        $player=$this->getDoctrine()->getRepository(Players::class)->find($_SESSION['idgracza']);
        $doesDrink=$player->getDrinksNow();
        if ($doesDrink==1){
            $player->setDrinksNow(0);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();
            
        }
        $doesDrinkJSON= json_encode($doesDrink);
        
        }
        return new Response ($doesDrinkJSON);
    }
    
    /**
     * @Route("/room/{id}/game/isNewCard", name="newCard")
     * @Method({"GET", "POST"})
     */
    public function isNewCard(Request $request){
        //session_start();
        //$session = new Session(new PhpBridgeSessionStorage());
        //$session->start();
        $new=1;
        if(isset($_SESSION['idgracza'])){
            //echo $_SESSION['idgracza'];
        $me = $this->getDoctrine()->getRepository(Players::class)->find($_SESSION['idgracza']);
        $new = $me->getHasSeen();
        }
        //$new = $_SESSION['idgracza'];
        $newJSON = json_encode($new);
        
        return new Response($newJSON);
    }
    /**
     * @Route("/drinkOneMore")
     * @Method({"GET", "POST"})
     */
    public function drinkOneMore(Cards $onecard){
//        $session = new Session(new PhpBridgeSessionStorage());
//        $session->start();
        $onecard->drinkMore($_SESSION['idgracza']);
        return new Response();
    }
    /*
     * @Route("/drinkOnJack")
     * @Method({"GET", "POST"})
     */
    public function drink(Cards $onecard){
        $onecard->drink($_SESSION['idgracza']);
        return new Response();
    }


    /**
     * @Route("/room/{id}/game/start", name="gamestart")
     * @Method({"GET", "POST"})
     */
    public function gameStart($id){
        $room=$this->getDoctrine()->getRepository(Rooms::class)->find($id);
        $start = $room->getIsStarted();
        $startJSON= json_encode($start);
        return new Response($startJSON);
    }
    /**
     * @Route("/room/{id}/game/isReady", name="isReady")
     * @Method({"GET", "POST"})
     */
    public function isReady($id){
        $players = $this->getDoctrine()->getRepository(Players::class)->findByID($id);
        $isReady=1;
        foreach ($players as $player){
            if ($player->getIsReady()==0){
                $isReady=0;
            }
        }
        $isReadyJSON = json_encode($isReady);
        return new Response($isReadyJSON);
    }
    /**
     * @Route("/room/{id}/game/changeIsReady", name="changeIsReady")
     * @Method({"GET", "POST"})
     */
    public function changeIsReady(){
//        $session = new Session(new PhpBridgeSessionStorage());
//        $session->start();
        $player=$this->getDoctrine()->getRepository(Players::class)->find($_SESSION['idgracza']);
        $player->setIsReady(1);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->flush();
        return new Response();
        
    }
       /**
     * @Route("/room/{id}/view/json",  requirements={"id"="\d+"})
     * @Method({"GET", "POST"})
     */
    public function jsonRoom($id){
        
        $players=$this->getDoctrine()->getRepository(Players::class)->findById($id);
        
        $playersJSON = json_encode($players);
        return new Response ( $playersJSON);
        
    }

}
