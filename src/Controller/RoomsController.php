<?php

namespace App\Controller;

use App\Entity\Players;
use App\Entity\Rooms;
use App\Utils\Cards;
use App\Utils\Game;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\PhpBridgeSessionStorage;


class RoomsController extends Controller {
     /**
     * @Route("/zasady", name="zasady")
     * @Method({"GET", "POST"})
     */
    
    public function zasady(){
        
            return $this->render('zasady.html.twig');
        
    }
    
    /**
     * @Route("/room/new")
     * @Method({"GET", "POST"})
     */
    public function newRoom(Request $request){
//        $session = new Session(new PhpBridgeSessionStorage());
//        $session->start();
        //$session->set('id', 150);
       
        $room = new Rooms();
        $name=$request->get('name');
        $password=$request->get('password');
        if($name!=null) {
          $room->setRoomName($name);
          $room->setPassword($password);
          
          
          $player = $this->getDoctrine()->getRepository(Players::class)->find($_SESSION['idgracza']);
          $room->setAdmin($player);
          $room->setIsStarted(0);
          $entityManager = $this->getDoctrine()->getManager();

          $entityManager->persist($room);
          
          
          $entityManager->flush();
          $ID = $room->getId();
          $player->setWhichRoom($room);
          $entityManager->flush();
          
//          $session->set('loged', true);
//          $session->set('correctId', $ID);
          $_SESSION['loged']=true;
          $_SESSION['correctId']=$ID;
          //return new Response();
          return $this->redirectToRoute('thisroom', array('id'=>$ID));
    }
   return $this->render('newroom.html.twig'
        );
    }
        /**
   * @Route("/room", name="rooms")
   * #Method({"GET"})
   */
  public function roomsList(){
    

    $players= $this->getDoctrine()->getRepository(Players::class)->findAll();
    
    $rooms= $this->getDoctrine()->getRepository(Rooms::class)->findAll();
    
    for ( $i=0, $x= count($rooms)-1; $i<count($rooms); $i++, $x--){
        $rooms_tmp[$i] = $rooms[$x]; 
    }

    
   
    
    return $this->render('roomlist.html.twig', array('rooms'=>$rooms_tmp, 'players'=>$players));
  }
  
  
  


    /**
     * @Route("/room/{id}",  requirements={"id"="\d+"}, name="thisroom")
     * @Method({"GET", "POST"})
     */
    public function joinRoom(Request $request, $id){
        
        $room = $this->getDoctrine()->getRepository(Rooms::class)->find($id);
        $players = $this->getDoctrine()->getRepository(Players::class)->findByID($id);
        $currentPlayer = $this->getDoctrine()->getRepository(Players::class)->find($_SESSION['idgracza']);
        
        if ($_SESSION['loged']==false || $_SESSION['correctId']!=$id){
            $password = $request->get('password');
            $currentId = $_SESSION['idgracza'];
            
            if ( $password==$room->getPassword() && $password!=NULL){
//                $session->set('correctId', $id);
//                $session->set('loged', true);
                $_SESSION['correctId']=$id;
                $_SESSION['loged']=TRUE;
                $this->getDoctrine()->getRepository(Players::class)->find($currentId)->setWhichRoom($room);
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->flush();
        
                if($room->getIsStarted()==1){
                    return $this->redirectToRoute('game', array('id'=>$id));
                }
                
        
                return $this->render('/playerlist.html.twig',  array('players'=>$players, 'room'=>$room, 'id'=>$currentPlayer 
                ));
                
            }
            else {
                if($password==null){
                    $correct = 1;
                    return $this->render('/password.html.twig', array('correct'=>$correct, 'room'=>$room));
                }
                else {
                $correct = 0;
                return $this->render('/password.html.twig', array('correct'=>$correct, 'room'=>$room));
                }
            }
        }
        else {
            if($room->getIsStarted()==1){
                return $this->redirectToRoute('game', array('id'=>$id) );
            }  
            return $this->render('/playerlist.html.twig',  array('players'=>$players, 'room'=>$room, 'id'=>$currentPlayer 
                ));
        }
    }

        /**
     * @Route("/room/{id}/game", name="game")
     * @Method({"GET", "POST"})
     */
    
    public function game($id, Cards $oneCard, Game $game, Request $request){
        
        
        $card_action = 1;
        $room = $this->getDoctrine()->getRepository(Rooms::class)->find($id);
        $entityManager = $this->getDoctrine()->getManager();
        $roomID = $room->getId();
        $players = $this->getDoctrine()->getRepository(Players::class)->findByID($id);
      
        
        if ( $room->getGameEnd()==1){
            
            return $this->redirectToRoute('end',array('id'=>$id) ); 
        }
        else {
         
        
//            $session = new Session(new PhpBridgeSessionStorage());
//            $session->start();
            $card = $game->getCurrentCard($id);
            //$myID= $session->get('idgracza')
            $myID = $_SESSION['idgracza'];
         
            $me=$this->getDoctrine()->getRepository(Players::class)->find($myID);



            $wasNow=0;
            $CP = $room->getCurrentPlayer();
            $PP = $room->getPreviousPlayer();

            $name=$CP->getName();
            if($myID==$CP->getId()){
                $isNow=1;
            }
            else {$isNow=0;}
            if($PP!=null){
                if ( $myID==$PP->getID()){


    //                $room->setPreviousPlayer(null);
    //                $entityManager->flush();
                    $wasNow=1;
                }
            }
            
            if ($card!=NULL){


                $image = $oneCard->getCardImage($card->cardId, $card->color);
                $image=$image.".svg";
                $C_ID = $card->cardId;
                //$C_ID = 3;
                if ( $C_ID=="J"){
                    $C_ID=11;
                }
                if ( $C_ID=="Q"){
                    $C_ID=12;
                }
                if ( $C_ID=="K"){
                    $C_ID=13;
                }
                if ( $C_ID=="A"){
                    $C_ID=14;
                }
                if($me->getHasSeen()==0){
                    $me->setHasSeen(1);
                    
                    //echo "cos ";

                    if($C_ID==3 || $C_ID==5 || $C_ID==6 || $C_ID==13 || $C_ID==12 || $C_ID==14){
                        //echo "cos2 ";
                        $oneCard->action($C_ID);

                    }
                    $entityManager->flush();
                }

            }
            else{

                $image="back-navy.png";
                $C_ID = 0;
                
                foreach ($players as $player ){
                    $player->setDrinks(0);
                }
                $entityManager->flush();
            }
            $Q = $this->getDoctrine()->getRepository(Rooms::class)->find($id)->getWhoQueen();
            if ( $C_ID==8){
                foreach ($players as $player){
                    $exist=0;
                    for ( $x = 0; $x<count($players); $x++){
                        if($player==$players[$x]->getEight()){
                            $exist=1;
                        }
                    }
                    if( $exist==0){
                        $players8[]=$player;
                    }
                }
                $players=$players8;
            }
        }
        return $this->render('/game.html.twig', ['image' => $image, 
            'isNow' => $isNow, 
            'name'=>$name, 
            'Q' => $Q, 
            'myID' => $myID, 
            'players' => $players,
            'cardID' => $C_ID,    
            'roomID' => $roomID,
            'wasNow' => $wasNow
            ]);
    }
    /**
     * @Route("/room/{id}/game/end", name="end")
     * @Method({"GET", "POST"})
     */
    public function gameEnd($id){
      
       $players = $this->getDoctrine()->getRepository(Players::class)->findByIdDrinkOrder($id);

       
          
       return $this->render('/end.html.twig',  array('players'=>$players, 'id'=>$id));
    }
 
    
    
   
}

