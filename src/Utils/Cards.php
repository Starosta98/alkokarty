<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Utils;

use App\Entity\Players;
use App\Entity\Rooms;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\PhpBridgeSessionStorage;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Description of Cards
 *
 * @author Maciek
 */
class Cards {
     private $em;
    

    
    public function __construct(EntityManagerInterface $em){
       $this->em = $em;
   

       
    }
    function drink($id){
        
        $pl = $this->em->getRepository(Players::class)->find($id);
        $pl->setDrinks($pl->getDrinks()+1);
        $pl->setDrinksNow(1);
        $pl->setIsReady(0);
        $pl8=$pl->getEight();
        if( $pl8!=null){
            
            $pl8->setDrinks($pl8->getDrinks()+1);
            $pl8->setDrinksNow(1);
            $pl8->setIsReady(0);
        }
        $this->em->flush();
    }
    function drinkMore($id){
        
        $pl = $this->em->getRepository(Players::class)->find($id);
        $pl->setDrinks($pl->getDrinks()+1);
      
        $pl->setIsReady(0);
        $pl8=$pl->getEight();
        if( $pl8!=null){
            $pl8->setDrinks($pl8->getDrinks()+1);
            
            $pl8->setIsReady(0);
        }
        $this->em->flush(); 
    }
   
            
    function action($cardId, $playerID=null){
      
        $currentPlayer = $this->em->getRepository(Players::class)->find($_SESSION['idgracza']);
        
        $room=$currentPlayer->getWhichroom();
        $players = $this->em->getRepository(Players::class)->findByID($room->getId());
        $previusPlayer = $room->getPreviousPlayer();
        $CP = $room->getCurrentPlayer();
        
        $this->em->flush();
        if ( $cardId==13 || $cardId ==14){
            $currentPlayer->setIsReady(1);
            $this->em->flush();
            
        }
        if ($cardId==2 || $cardId==4 || $cardId == 7 || $cardId == 9 || $cardId == 10){
           
//            $players = $this->em->getRepository(Players::class)->findByID($currentPlayer->getWhichroom());
//            //W magicznzy sposób pokaż listę graczy 
//            $id=$players[0]->getId();
//            $id=20;
            if($playerID!=null){

            $this->drink($playerID);
            }
            foreach ($players as $player){
                if($playerID!=$player->getId()){
                    $player->setIsReady(1);
                }
            }
            $this->em->flush();
            
        }
        if ( $cardId == 3 ){
           
            if ( $currentPlayer->getId()==$previusPlayer->getId()){
               
                $this->drink ($currentPlayer->getId());
                
            }
            else{
                
                $currentPlayer->setIsReady(1);
                $this->em->flush();
            }
            
        }
        if ( $cardId == 5){
            //die("chuj");
            if($currentPlayer->getSex()=="M"){
//                $myID = $currentPlayer->getId();
//                die("$myID");
                $this->drink($currentPlayer->getId());
                
            }
            else {
                $currentPlayer->setIsReady(1);   
            }
            $this->em->flush();
        }
        if ($cardId ==6){
            if($currentPlayer->getSex()=="K"){
                
                $this->drink($currentPlayer->getId());

                
            }
            else{
               $currentPlayer->setIsReady(1); 
            }
            $this->em->flush();
        }
//           
        if($cardId== 8){
            

            $pl8 = $this->em->getRepository(Players::class)->find($playerID);
            $currentPlayer->setEight($pl8);
            foreach ($players as $player){
                
                    $player->setIsReady(1);
                
            }
//            
            $this->em->flush();
        }
        if ( $cardId == 11){
            //$players = $this->em->getRepository(Players::class)->findByID($currentPlayer->getWhichroom());
            //Lista wielokrotnego wyboru i dostaje $listId
            if($playerID!=null) {
            foreach ($players as $player){
                
                    $player->setIsReady(1);
                
            }
            for ( $x=0; $x<count($playerID); $x++){
                $this->drink($playerID[$x]);
            }
            }
        }
        if ( $cardId == 12){
            
            $room->setWhoQueen($previusPlayer);
           
            $currentPlayer->setIsReady(1);
            $this->em->flush();
        }
        }
        
        public function getCardImage($cardId, $cardColor) {
        
        if ($cardColor==0){
            $c="C";
        }
        if($cardColor==1){
            $c="H";
        }
        if($cardColor==2){
            $c="D";
        }
        if($cardColor==3){
            $c="S";
        }
        return $cardId . $c;
    }
}

