<?php

namespace App\Utils;

use App\Entity\Players;
use App\Entity\Rooms;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\PhpBridgeSessionStorage;
use Doctrine\ORM\EntityManagerInterface;

class Game{
    var $fulldeck;
    var $deck;
    private $em;
    

    
    public function __construct(EntityManagerInterface $em){
       $this->em = $em;
   

       
    }
    public function start($count) {
        $deck = new Stos();
        $fulldeck = [];
//        for ( $x=0; $x<52; $x++){
//            $fulldeck[$x]=$x+1;
//        }
        for ( $x=0; $x<4;$x++){
            $fulldeck[$x]=new Two;
            $fulldeck[$x]->color=($x%4);
        }
        for ( $x=4; $x<8;$x++){
            $fulldeck[$x]=new Three;
            $fulldeck[$x]->color=($x%4);
        }
        for ( $x=8; $x<12;$x++){
            $fulldeck[$x]=new Four;
            $fulldeck[$x]->color=($x%4);
        }
        for ( $x=12; $x<16;$x++){
            $fulldeck[$x]=new Five;
            $fulldeck[$x]->color=($x%4);
        }
        for ( $x=16; $x<20;$x++){
            $fulldeck[$x]=new Six;
            $fulldeck[$x]->color=($x%4);
        }
        for ( $x=20; $x<24;$x++){
            $fulldeck[$x]=new Seven;
            $fulldeck[$x]->color=($x%4);
        }
        for ( $x=24; $x<28;$x++){
            $fulldeck[$x]=new Eight;
            $fulldeck[$x]->color=($x%4);
        }
        for ( $x=28; $x<32;$x++){
            $fulldeck[$x]=new Nine;
            $fulldeck[$x]->color=($x%4);
        }
        for ( $x=32; $x<36;$x++){
            $fulldeck[$x]=new Ten;
            $fulldeck[$x]->color=($x%4);
        }
        for ( $x=36; $x<40;$x++){
            $fulldeck[$x]=new Jack;
            $fulldeck[$x]->color=($x%4);
        }
        for ( $x=40; $x<44;$x++){
            $fulldeck[$x]=new Queen;
            $fulldeck[$x]->color=($x%4);
        }
        for ( $x=44; $x<48;$x++){
            $fulldeck[$x]=new King;
            $fulldeck[$x]->color=($x%4);
        }
        for ( $x=48; $x<52;$x++){
            $fulldeck[$x]=new Ace;
            $fulldeck[$x]->color=($x%4);
        }
//        for ( $x=0; $x<52; $x++){
//            echo $fulldeck[$x];
//        }
        
            
        for ( $i=0; $i<$count; $i++){
            do{
                 $randIndex = rand(0,count($fulldeck)-1);
                 
            }
            while($fulldeck[$randIndex]== NULL);
           
            
            $deck->push($fulldeck[$randIndex]);
            
            $fulldeck[$randIndex]=NULL;
        }
//        $deck->push($fulldeck[5]);
//        $deck->push($fulldeck[6]);
//        $deck->push($fulldeck[7]);
        
        $this->fulldeck=$fulldeck;
        $this->deck=$deck;
    }
    public function click($id){
        $deckDB = $this->em->getRepository(Rooms::class)->find($id)->getGame();
        $deck = unserialize($deckDB);
        
        $room = $this->em->getRepository(Rooms::class)->find($id);
        if ($deck->isEmpty()){
            $room->setGameEnd(1);
            $room->setIsStarted(0);   
            $room->setGame(null);
            $room->setCurrentCard(null);
            $room->setCurrentPlayer(null);
            $room->setPreviousPlayer(null);
            $room->setWhoQueen(null);
            
            $players = $this->em->getRepository(Players::class)->findByID($id);
            foreach ($players as $player){
                $player->setEight(NULL);
                $player->setHasSeen(0);
                $player->setIsReady(0);
                $this->em->flush();
        }
            
        }
        else{
            $card = $deck->pop();
            $jsonContent = serialize($deck);
            $jsonCard = serialize($card);
            $room->setGame($jsonContent);
            $room->setCurrentCard($jsonCard);
           
            $CP = $room->getCurrentPlayer();
            $players = $this->em->getRepository(Players::class)->findByID($id);
            foreach ($players as $player){
                $player->setHasSeen(0);
                $player->setIsReady(0);
                $this->em->flush();
            }

            if($CP==$players[count($players)-1]){

                $CP_new=$players[0];
            }
            else {
                for ( $x=0; $x<count($players)-1; $x++){
                    if ( $players[$x]===$CP){
                        $CP_new = $players[$x+1];
                    }
                }
            }

            $room->setCurrentPlayer($CP_new);
            $room->setPreviousPlayer($CP);
            $this->em->flush();
        }
        //return $card;
    }
    public function getCurrentCard($id){
        $cardDB = $this->em->getRepository(Rooms::class)->find($id)->getCurrentCard();
        $card = unserialize($cardDB);
        return $card;
    }
}